import {NavLink} from "react-router-dom";

const AppNave = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
            <div className="container">
                <ul className="navbar-nav ml-auto">
                    <li className="nav-item">
                        <NavLink to="/" className="nav-link">Income</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink to="/expanse" className="nav-link">Expense</NavLink>
                    </li>
                </ul>

            </div>
        </nav>
    );
};

export default AppNave;