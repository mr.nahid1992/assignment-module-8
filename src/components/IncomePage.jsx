import { useState } from "react";
import AppNave from "./AppNave.jsx";



const IncomePage = () => {


    const [income,setIncome]=useState("");
    const [incomeList,setIncomeList]=useState([]);


    const handelIncomeChange=(event)=>{
        setIncome(parseInt(event.target.value));
    }
    
    const handleIncomeSubmit =(event) =>{
        event.preventDefault();
        if(!income) return;
        const newIncome={
            id:Date.now().toString(),
            income:income,
        } 
        setIncomeList([...incomeList,newIncome]);
        setIncome('');
    }


    return (
        <div>
            <AppNave/>
            <h1>Income Page</h1>
            <form onSubmit={handleIncomeSubmit}>
                <input type="number" value={income} onChange={handelIncomeChange} />
                <button type="submit" className="">+</button>
            </form>
            <div className="todo-container">
                <ul className="todo-list">
                    {
                    incomeList.map((task) => (
                        <div key={task.id}  >
                            <li key={task.id} className="todo-item">
                            {task.income} 
                            </li>
                        </div>
                    
                    ))
                    }
                </ul>
            </div>
        </div>
    );
};

export default IncomePage;