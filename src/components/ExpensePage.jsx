import { useState } from "react";
import AppNave from "./AppNave.jsx";

const ExpensePage = () => {
    const [expanceList,setExpanceList]=useState([]);
    const [expence,setExpance]=useState('');

    const handelExpanceChange=(event)=>{
        setExpance(parseInt(event.target.value));
    }
    const handleExpanceSubmit=(event)=>{
        event.preventDefault();
        if(!expence) return;
        const newExpance={
            id:Date.now().toString(),
            expance:expence,
        } 
        setExpanceList([...expanceList,newExpance]);
        setExpance('');

    }

    return (
        <div>
            <AppNave/>
            <h1>Expance Page</h1>
            <form onSubmit={handleExpanceSubmit}>
                <input type="number" value={expence} onChange={handelExpanceChange} />
                <button type="submit" className="">+</button>
            </form>
            <div className="todo-container">
                <ul className="todo-list">
                    {
                    expanceList.map((task) => (
                        <div key={task.id}  >
                            <li key={task.id} className="todo-item">
                            {task.expance} 
                            </li>
                        </div>
                    
                    ))
                    }
                </ul>
            </div>
        </div>
    );
};

export default ExpensePage;