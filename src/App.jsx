import './App.css'
import {BrowserRouter, Route, Routes} from "react-router-dom";

import IncomePage from "./components/IncomePage.jsx";
import ExpensePage from "./components/ExpensePage.jsx";

function App() {


    return (
        <div>
            <BrowserRouter>
                <Routes>
                    <Route path="/" element={<IncomePage/>}/>
                    <Route path="/expanse" element={<ExpensePage/>}/>
                </Routes>
            </BrowserRouter>
        </div>


    )
}

export default App
